extern crate serde;
#[macro_use] extern crate serde_derive;

use std::collections::HashMap;

#[derive(Deserialize)]
struct TestType<'a> {
    test: HashMap<&'a str, &'a str>,
}
